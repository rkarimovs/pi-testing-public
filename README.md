OpenSSL is needed, install with:
`yum install openssl openssl-devel -y` OR `apt-get install openssl openssl-devel -y` (or adjust to whatever pm your distro has)

clone this repo and run with:
- `g++ main -lssl -lcrypto`
- `./a.out`