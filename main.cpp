#include <iostream>
#include <string>
#include <map>
#include <regex>
#include <stdexcept>
#include <openssl/sha.h>
#include <openssl/rand.h>

class UserRegistration {
private:
    std::map<std::string, std::pair<std::string, std::string>> userDatabase; //Simulated user database (username -> (email, password))

public:
    //Function to register a new user
    void registerUser(const std::string& username, const std::string& email, const std::string& password) {
        // Input validation
        if (username.empty()) {
            throw std::invalid_argument("Username cannot be empty");
        }
        if (!std::regex_match(email, std::regex("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}"))) {
            throw std::invalid_argument("Invalid email format");
        }
        if (password.length() < 10 || password.length() > 30) {
            throw std::invalid_argument("Password must be between 10 and 30 characters long");
        }
        if (!containsUppercase(password) || !containsLowercase(password) || !containsNumber(password) || !containsSpecialCharacter(password)) {
            throw std::invalid_argument("Password must contain at least one uppercase letter, one lowercase letter, one digit, and one special character");
        }

        //Generate salt
        std::string salt = generateSalt();

        //Hash password
        std::string hashedPassword = hashPassword(password, salt);

        std::string saltHash = salt + ":" + hashedPassword;

        //Simulate database ops
        if (userDatabase.find(username) != userDatabase.end()) {
            throw std::runtime_error("Username already exists");
        }
        userDatabase[username] = std::make_pair(email, saltHash);
        std::cout << "User registered successfully.\n";
    }

    //Function to print contents of database
    void printUserDatabase() {
        std::cout << "User Database Contents:" << std::endl;
        for (const auto& user : userDatabase) {
            std::cout << "Username: " << user.first << ", Email: " << user.second.first << ", Password: " << user.second.second << std::endl;
        }
    }


private:
    //Function to generate a random salt
    std::string generateSalt() {
        unsigned char salt[16];
        RAND_bytes(salt, sizeof(salt));
        char hexSalt[33];
        for (int i = 0; i < 16; ++i) {
            sprintf(hexSalt + i * 2, "%02x", salt[i]);
        }
        return std::string(hexSalt);
    }

    //Function to hash the password with salt
    std::string hashPassword(const std::string& password, const std::string& salt) {
        std::string saltedPassword = password + salt;
        unsigned char hash[SHA256_DIGEST_LENGTH];
        SHA256((unsigned char*)saltedPassword.c_str(), saltedPassword.length(), hash);
        char hexHash[SHA256_DIGEST_LENGTH * 2 + 1];
        for (int i = 0; i < SHA256_DIGEST_LENGTH; ++i) {
            sprintf(hexHash + i * 2, "%02x", hash[i]);
        }
        return std::string(hexHash);
    }

    //Function to check if the password contains an uppercase letter
    bool containsUppercase(const std::string& password) {
        for (char c : password) {
            if (std::isupper(c)) {
                return true;
            }
        }
        return false;
    }

    // Function to check if the password contains a lowercase letter
    bool containsLowercase(const std::string& password) {
        for (char c : password) {
            if (std::islower(c)) {
                return true;
            }
        }
        return false;
    }

    // Function to check if the password contains a digit
    bool containsNumber(const std::string& password) {
        for (char c : password) {
            if (std::isdigit(c)) {
                return true;
            }
        }
        return false;
    }

    // Function to check if the password contains a special character
    bool containsSpecialCharacter(const std::string& password) {
        for (char c : password) {
            if (!std::isalnum(c) && !std::isspace(c)) {
                return true;
            }
        }
        return false;
    }
};

int main() {
    UserRegistration registration;

    registration.printUserDatabase();

    return 0;
}