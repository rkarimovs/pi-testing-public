#include "pch.h"
#include "main.cpp"

class UserRegistrationTest : public ::testing::Test {
protected:
    UserRegistration userReg;
};

TEST_F(UserRegistrationTest, RegisterUserSuccess) {
    ASSERT_NO_THROW(userReg.registerUser("testuser", "testuser@example.com", "Password123!"));
}

TEST_F(UserRegistrationTest, RegisterUserEmptyUsername) {
    ASSERT_THROW(userReg.registerUser("", "testuser@example.com", "Password123!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUserInvalidEmail) {
    ASSERT_THROW(userReg.registerUser("testuser", "invalidemail", "Password123!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUserShortPassword) {
    ASSERT_THROW(userReg.registerUser("testuser", "testuser@example.com", "Pass12!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUserLongPassword) {
    ASSERT_THROW(userReg.registerUser("testuser", "testuser@example.com", "Password1234567890123456789012345678901!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUserNoUppercase) {
    ASSERT_THROW(userReg.registerUser("testuser", "testuser@example.com", "password123!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUserNoLowercase) {
    ASSERT_THROW(userReg.registerUser("testuser", "testuser@example.com", "PASSWORD123!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUserNoDigit) {
    ASSERT_THROW(userReg.registerUser("testuser", "testuser@example.com", "Password!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUserNoSpecialCharacter) {
    ASSERT_THROW(userReg.registerUser("testuser", "testuser@example.com", "Password123"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUserUsernameExists) {
    userReg.registerUser("testuser", "testuser@example.com", "Password123!");
    ASSERT_THROW(userReg.registerUser("testuser", "testuser2@example.com", "Password123!"), std::runtime_error);
}

TEST_F(UserRegistrationTest, RegisterUser_EmptyUsername) {
    // Test empty username
    EXPECT_THROW(userReg.registerUser("", "john.doe@example.com", "StrongPassword123!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUser_InvalidEmailFormat) {
    // Test invalid email format
    EXPECT_THROW(userReg.registerUser("john_doe", "invalid_email_format", "StrongPassword123!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUser_ShortPassword) {
    // Test short password
    EXPECT_THROW(userReg.registerUser("john_doe", "john.doe@example.com", "Short1!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUser_LongPassword) {
    // Test long password
    EXPECT_THROW(userReg.registerUser("john_doe", "john.doe@example.com", "ThisIsAVeryLongPasswordThatExceedsTheMaximumLengthOfThirtyCharacters1!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUser_WeakPassword_NoUppercase) {
    // Test weak password - no uppercase letter
    EXPECT_THROW(userReg.registerUser("john_doe", "john.doe@example.com", "weakpassword123!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUser_WeakPassword_NoLowercase) {
    // Test weak password - no lowercase letter
    EXPECT_THROW(userReg.registerUser("john_doe", "john.doe@example.com", "WEAKPASSWORD123!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUser_WeakPassword_NoDigit) {
    // Test weak password - no digit
    EXPECT_THROW(userReg.registerUser("john_doe", "john.doe@example.com", "WeakPassword!"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUser_WeakPassword_NoSpecialCharacter) {
    // Test weak password - no special character
    EXPECT_THROW(userReg.registerUser("john_doe", "john.doe@example.com", "WeakPassword123"), std::invalid_argument);
}

TEST_F(UserRegistrationTest, RegisterUser_DuplicateUsername) {
    // Test duplicate username
    userReg.registerUser("john_doe", "john.doe@example.com", "StrongPassword123!");
    EXPECT_THROW(userReg.registerUser("john_doe", "another_email@example.com", "AnotherStrongPassword123!"), std::runtime_error);
}

TEST_F(UserRegistrationTest, RegisterUser_DuplicateEmail) {
    // Test duplicate email
    userReg.registerUser("john_doe", "john.doe@example.com", "StrongPassword123!");
    EXPECT_THROW(userReg.registerUser("john_doe2", "john.doe@example.com", "AnotherStrongPassword123!"), std::runtime_error);
}